package com.ntels.portal.controller;

import java.io.IOException;
import java.util.Locale;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ntels.portal.domain.GroupData;

@Controller
public class BoardController {

	
	@RequestMapping(value = "/managerList", method = RequestMethod.GET)
	public String list(Locale locale, Model model) {
		return "portal/manager/managerList";
	}
	
	@PostMapping("/api/user-count")
	public void groupData(@RequestBody String [] data) throws IOException {
		
	}
}
