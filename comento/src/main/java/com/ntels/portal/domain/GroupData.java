package com.ntels.portal.domain;

import lombok.Data;

@Data
public class GroupData {
	public String code;
	public String message;
}
