package com.ntels.portal.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ntels.portal.dao.SampleMapper;
import com.ntels.portal.domain.Group;

@Service
public class BoardService {
	@Autowired
	private SampleMapper sampleMapper;
	
	public List<Group> userCountByName(String group_name){
		return sampleMapper.getGroupByName(group_name);
	}
}
