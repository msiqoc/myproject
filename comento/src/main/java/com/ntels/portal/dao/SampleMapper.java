package com.ntels.portal.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.ntels.portal.domain.Group;

@Component
public interface SampleMapper {
	public String getSampleData();

	public List<Group> getGroupByName(String group_name);
}
